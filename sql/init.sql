
create table "Comments" (
    id serial not null,
    "movieId" integer not null,
    content text not null,
    "createdAt" timestamp not null,
    "updatedAt" timestamp not null
);

create table "Movies" (
    id serial not null,
    title varchar(256) not null,
    year integer not null,
    rated varchar(16) not null,
    released timestamp not null,
    runtime varchar(16) not null,
    genre varchar(32)not null,
    director varchar(256) not null,
    writer text not null,
    actors text not null,
    plot text not null,
    language varchar(256) not null,
    country varchar(256) not null,
    awards text not null,
    poster varchar(256) not null,
    ratings json not null,
    metascore integer not null,
    "imdbRating" float not null,
    "imdbVotes" varchar(256) not null,
    "imdbID" varchar(256) not null,
    type varchar(64) not null,
    dvd timestamp not null,
    "boxOffice" varchar(256) not null,
    production varchar(256) not null,
    website varchar(256) not null,
    "createdAt" timestamp not null,
    "updatedAt" timestamp not null
)

