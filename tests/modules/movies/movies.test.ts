//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

//Require the dev-dependencies
import app from '../../../app';
import * as chai from 'chai';
import * as chaiHttp from 'chai-http';
import db from '../../../features/database';

const expect = chai.expect;

chai.use(chaiHttp);
// in real-life usage it would be a separated database
describe('Movies', () => {
    before((done) => { //Before each test we empty the database
        db._models.Movie.destroy({
            truncate: true,
            restartIdentity: true
        }).then(done.bind(this)).catch((err) => {
            done();
        });
    });
    /*
      * Test the /GET route
      */
    describe('/POST movies', () => {
        it('it should create a movie', (done) => {
            let movie = {
                title: 'Star Wars',
            };
            chai.request(app)
                .post('/movies')
                .send(movie)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.have.property('title');
                    expect(res.body).to.have.property('year');
                    expect(res.body).to.have.property('rated');
                    expect(res.body).to.have.property('released');
                    expect(res.body).to.have.property('runtime');
                    expect(res.body).to.have.property('genre');
                    expect(res.body).to.have.property('director');
                    expect(res.body).to.have.property('writer');
                    expect(res.body).to.have.property('actors');
                    expect(res.body).to.have.property('plot');
                    expect(res.body).to.have.property('language');
                    expect(res.body).to.have.property('country');
                    expect(res.body).to.have.property('awards');
                    expect(res.body).to.have.property('poster');
                    expect(res.body).to.have.property('ratings');
                    expect(res.body).to.have.property('metascore');
                    expect(res.body).to.have.property('imdbRating');
                    expect(res.body).to.have.property('imdbVotes');
                    expect(res.body).to.have.property('imdbID');
                    expect(res.body).to.have.property('type');
                    expect(res.body).to.have.property('dvd');
                    expect(res.body).to.have.property('boxOffice');
                    expect(res.body).to.have.property('production');
                    expect(res.body).to.have.property('website');
                    done();
                });
        });
    });

    describe('/GET movies', () => {
        it('it should GET all the movies', (done) => {
            chai.request(app)
                .get('/movies')
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('array');
                    expect(res.body.length).to.be.equals(1);
                    done();
                });
        });
    });

});