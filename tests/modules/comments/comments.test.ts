//During the test the env variable is set to test
import {IComment} from "../../../features/models/comments/comments-interface";

process.env.NODE_ENV = 'test';

//Require the dev-dependencies
import app from '../../../app';
import * as chai from 'chai';
import * as chaiHttp from 'chai-http';
import db from '../../../features/database';

const expect = chai.expect;

chai.use(chaiHttp);
// in real-life usage it would be a separated database
describe('Comment', () => {
    before((done) => { //Before each test we empty the database
        db._models.Comment.destroy({
            truncate: true,
            restartIdentity: true
        }).then(done.bind(this)).catch((err) => {
            done();
        });
    });
    /*
      * Test the /GET route
      */
    describe('/POST comments', () => {
        it('it should create a comment', (done) => {
            let comment: IComment = {
                movieId: 1,
                content: 'This is some random comment.'
            };
            chai.request(app)
                .post('/comments')
                .send(comment)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.have.property('id');
                    expect(res.body).to.have.property('movieId');
                    expect(res.body).to.have.property('content');
                    done();
                });
        });
    });

    describe('/GET comments/:id', () => {
        it('it should GET given movie', (done) => {
            chai.request(app)
                .get('/comments/1')
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.have.property('id');
                    expect(res.body).to.have.property('movieId');
                    expect(res.body).to.have.property('content');
                    done();
                });
        });
    });

});