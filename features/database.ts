import * as SequelizeStatic from 'sequelize';
import {Sequelize} from "sequelize";
import * as fs from "fs";
import * as path from "path";

interface IDatabase {}

class Database implements IDatabase {
    private _sequelize: Sequelize;
    private _basename: any;
    _models: any = {};



    constructor() {

        this._sequelize = new SequelizeStatic(process.env.POSTGRES_DB, process.env.POSTGRES_USER, process.env.POSTGRES_PASSWORD, {
            host: process.env.POSTGRES_HOST,
            dialect: 'postgres',
        });

        const modelsPath = path.join(__dirname, '../features/models/');

        fs.readdirSync(modelsPath)
            .filter((file: string) => {
                return (file !== this._basename);
            })
            .map((file: string) => {
                if (fs.statSync(path.join(modelsPath, file)).isDirectory()) {
                    // traverse subdirectories to look for models
                    return fs.readdirSync(path.join(modelsPath, file))
                        .map((childFile: string) => {
                            // return subdir + fileName
                            return path.join(file, childFile);
                        });
                } else {
                    return file;
                }
            })
            .reduce((acc: any, val: any) => acc.concat(val), [])
            .filter((file: string) => {
                // check if file contains -model (these files are transpiled)
                // since all models are contained in separate folders it will only
                // get the right files
                return /-model.js$/.test(file);
            })
            .forEach((file) => {
                const model = this._sequelize.import(path.join(modelsPath, file));
                this._models[(model as any).name] = model;
            });

        Object.keys(this._models).forEach((modelName: string) => {
            if (typeof this._models[modelName].associate === 'function') {
                this._models[modelName].associate(this._models);
            }
        });


    }

    get Models() {
        return this._models;
    }
}

export default new Database();