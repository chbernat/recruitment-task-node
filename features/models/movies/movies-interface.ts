
export interface IMovie {
    id?: number;
    title: string;
    year: number;
    rated: string;
    released: Date;
    runtime: string;
    genre: string;
    director: string;
    writer: string;
    actors: string;
    plot: string;
    language: string;
    country: string;
    awards: string;
    poster: string;
    ratings: any;
    metascore: number;
    imdbRating: number;
    imdbVotes: string;
    imdbID: string;
    type: string;
    dvd: Date;
    boxOffice: string;
    production: string;
    website: string;
};