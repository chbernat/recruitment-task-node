import * as request from 'request-promise';
import {NextFunction, Request, Response} from "express";

import db from '../../database';
import moviesHelper from "./movies.helper";
import response from '../../utils/response';
import {lowerCaseFirstLetterOfKeys} from "../../utils/object-keys-helper";
import {IMovie} from "../../models/movies/movies-interface";

export default class MoviesController {
    controller() {}

    getMovies(req: Request, res: Response, next: NextFunction): Promise<Array<IMovie>> {
        return db.Models.Movie
            .findAll()
            .then(response.send200.bind(this, res))
            .catch((err) => next(`Failed to fetch movie. Error message: ${err}`));
    }

    createMovie(req: Request, res: Response, next: NextFunction): void {
        const movieTitle: string = moviesHelper.validateMovieBody(req.body);

        if (!movieTitle) return next("No movie title was passed!");

        request
            .get(moviesHelper.buildOptions(movieTitle))
            .then(lowerCaseFirstLetterOfKeys)
            .then(this.findMovie.bind(this, next))
            .then(this.postMovieToDb.bind(this, next))
            .then(response.send200.bind(this, res))
            .catch((err) => next(`Failed to fetch movie from open database. Error message: ${err}`))
    }

    private postMovieToDb(next: NextFunction, movie: IMovie): Promise<IMovie> {
        return (movie.id) ? movie : db.Models.Movie
            .create(movie)
            .catch((err) => next(`Failed to create movie. Error message: ${err}`));
    }

    private findMovie(next: NextFunction, movie: IMovie): Promise<IMovie> {
        if (movie.toString() != "[object Object]") throw new Error('Something went wrong with open database!');
        return db.Models.Movie
            .findOne({where: {'imdbID': movie.imdbID}})
            .then((fetchedMovie: IMovie) => fetchedMovie || movie)
            .catch(err => next(`There was an error finding a movie in our database. Error message: ${err}`));
    }
}
