
import {RequestHandler, Router} from 'express';
import CommentsController from './comments.controller';
import {bindAll} from 'lodash';

const controller = new CommentsController();
const commentsRoutes = Router();

bindAll(controller, 'createComment', 'getComment');

commentsRoutes.get('/:id', controller.getComment);
commentsRoutes.post('/', controller.createComment);


export default commentsRoutes;